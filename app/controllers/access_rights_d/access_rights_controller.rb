module AccessRightsD
  class AccessRightsController < ApplicationController
    before_filter :init_variables
    skip_before_filter :check_access_rights, :only => [:no_access_rights]

    def index
      all_item(@entity, nil, true, true)
    end

    def show
      get_item_by_id(@entity, params[:id])
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id])
    end

    def create
      create_item(@entity, @path, params, @fields)
    end

    def update
      update_item(@entity, @path, params[:id], params, @fields)
    end

    def destroy
      delete_item(@entity, @path, params[:id])
    end

    def no_access_rights
    end

    private

    def init_variables
      @entity = AccessRight
      @param_name = :access_right
      # Поля
      @fields = {
          :system_user_role_id => {
              :type => :collection,
              :label => t('form.labels.system_user_role'),
              :show_in_table => true
          },
          :module_rights => {
              :type => :dual_list,
              :label => t('form.labels.dual_list'),
              :show_in_table => false,
              :recipient => {
                  :name => :module_rights,
                  :label => "Recipient",
                  :model => AccessRightsD::ModuleRight
              },
              :donor => {
                  :label => "Donor",
                  :model => ModulesD::ModuleItem,
                  :recipient_field => :access_right_id,
                  :donor_field => :module_item_id
              }
          }
      }
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
      @path = access_rights_d.access_rights_path
    end
  end
end
