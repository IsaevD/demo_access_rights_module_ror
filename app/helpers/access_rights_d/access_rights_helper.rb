module AccessRightsD
  module AccessRightsHelper

    def check_access_rights
      if signed_in(current_system_user)
        module_rights = AccessRightsD::ModuleRight.where(:system_user_role_id => current_system_user.system_user_role_id)
        module_info = params[:controller].split("/")
        module_item = ModulesD::ModuleItem.where(:module_name => module_info[0], :alias => module_info[1]).first
        if !module_item.nil?
          access_module = module_rights.where('id in (?)', module_item.id)
          if !access_module.nil?
            return true
          end
        else
          redirect_to access_rights_d.no_access_path
        end
      else
        redirect_to access_rights_d.no_access_path
      end
    end

  end
end