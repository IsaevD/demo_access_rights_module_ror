// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function(){
    initializeDualListBox();
});

// Инициализация полей типа DualListBox
function initializeDualListBox() {
    var dual_list_box_class = ".dual_list_box";
    var dual_list_box_from_class = ".dual_list_box .from";
    var dual_list_box_from_list_class = ".dual_list_box .from .values";
    var dual_list_box_from_element_class = ".dual_list_box .values li";
    var dual_list_box_to_class = ".dual_list_box .to";
    var dual_list_box_to_list_class = ".dual_list_box .to .values";
    var dual_list_box_to_element_class = ".dual_list_box .values li";
    var dual_list_box_add_btn_class = ".dual_list_box .btns .add_btn";
    var dual_list_box_remove_btn_class = ".dual_list_box .btns .remove_btn";
    var dual_list_box_param_name_class = ".dual_list_box #param_name";
    var dual_list_box_entity_class = ".dual_list_box #entity";

    $(dual_list_box_add_btn_class).click(function(){
        var entity = $(dual_list_box_entity_class).val();
        var param_name = $(dual_list_box_param_name_class).val();
        $(dual_list_box_from_element_class).children("input:checked").each(function(index, element){
            $(element).parent().appendTo(dual_list_box_to_list_class);
            $(element).parent().children("input[type=hidden]")
                .attr("name", param_name+"["+entity+"]["+$(element).attr("id")+"]")
                .attr("id", param_name+"_"+entity+"_"+$(element).attr("id"))
                .attr("disabled", false);
            $(element).attr("checked", false);
        });
    });
    $(dual_list_box_remove_btn_class).click(function(){
        $(dual_list_box_to_element_class).children("input:checked").each(function(index, element){
            $(element).parent().appendTo(dual_list_box_from_list_class);
            $(element).parent().children("input[type=hidden]").attr("name", "").attr("id", "").attr("disabled", true);
            $(element).attr("checked", false);
        });
    });
}