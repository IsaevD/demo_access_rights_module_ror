Rails.application.routes.draw do

  mount AccessRightsD::Engine => "/access_rights_d"
  mount ModulesD::Engine => "/modules_d"
  mount SystemMainItemsD::Engine => "/system_main_items_d"
  mount SystemUsersAuthorizeD::Engine => "/auth"
end
