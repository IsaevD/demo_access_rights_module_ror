# Импорт информации о модулях
modules = {
    "access_rights" => {
        :module_name => "access_rights_d",
        :name => "Модуль управления правами доступа",
        :description => "Модуль для назначения прав доступа на другие модули",
        :git_url => "https://denisisaevmich@bitbucket.org/denisisaevmich/access_rights_d.git"
    }
}
modules.each do |key, value|
  if (ModulesD::ModuleItem.find_by(:alias => key).nil?)
    ModulesD::ModuleItem.create({ :alias => key, :module_name => value[:module_name], :name => value[:name], :description => value[:description], :git_url => value[:git_url] })
  end
end
puts "AccessRightsD modules import success"
