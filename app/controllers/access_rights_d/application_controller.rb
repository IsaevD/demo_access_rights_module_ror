module AccessRightsD
  class ApplicationController < ActionController::Base
    include DataProcessingD::ApplicationHelper
    include ObjectGeneratorD::ApplicationHelper
    include SystemUsersAuthorizeD::ApplicationHelper
    include ApplicationHelper
    before_filter :check_access_rights
    helper ObjectGeneratorD::ApplicationHelper

  end
end
