AccessRightsD::Engine.routes.draw do
  resources :access_rights
  match '/no_access', to: "access_rights#no_access_rights", via: "get"
end
